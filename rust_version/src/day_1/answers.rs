use crate::read_file;

fn answer1(data: Vec<&str>) -> String {
    
    return "2".to_string();
}

fn answer2() -> String {
    return "not implemented".to_string();
}

pub fn day_1_answers() {
    let contents = match read_file::read("../../day_1/data/betdex.txt") {
        Ok(file) => file,
        Err(_error) => panic!("Cannot read file: "),
    };

    println!("{:?}", contents);
    // print!("Day 1 Answers:\n");
    // print!("\t1) {}\n", answer1(vec!["i", ""]));
    // print!("\t2) {}\n", answer2());
    // print!("\n");
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn day_1_question_1() {
        let test_data: Vec<&str> = vec![
            "1000",
            "2000",
            "3000",
            "",
            "4000",
            "",
            "5000",
            "6000",
            "",
            "7000",
            "8000",
            "9000",
            "",
            "10000",
        ];
        let result = answer1(test_data);
        assert_eq!(result, "24000");
    }
}