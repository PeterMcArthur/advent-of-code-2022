mod day_1;
mod read_file;

use day_1::answers::day_1_answers;
fn main() {
    day_1_answers();
}
