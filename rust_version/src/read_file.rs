use std::fs::File;
use std::io::{BufReader, BufRead, Error};

pub fn read(path: &str) -> Result<Vec<String>, Error> {

    let input = File::open(path)?;
    let buffered = BufReader::new(input);

    let mut result = Vec::new();

    for line in buffered.lines() {
        result.push(line?);
    }

    return Ok(result);
}