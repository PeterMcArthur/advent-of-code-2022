import fs from "fs";
import path from "path";

type ReadFile<T> = (path: string) => T;

export const toString: ReadFile<string> = (pathToData) => {
    try {
        return fs.readFileSync(path.join(__dirname, "../../..", pathToData), 'utf8');
    } catch (err) {
        console.error(err);
        throw err;
    }
};

export const toArray: ReadFile<string[]> = (path) => {
    try {
        return toString(path).split(/\n|\r/g);
    } catch (err) {
        console.error(err);
        throw err;
    }
}
