import * as day1 from "./day_1";
import * as day2 from "./day_2";

import * as readFile from "./utils/readFile";

console.log(`Day 1, Question 1)`, day1.answer1(readFile.toArray('./data/day_1/betdex.txt')));
console.log(`Day 1, Question 2)`, day1.answer2(readFile.toArray('./data/day_1/betdex.txt')));

console.log(`Day 2, Question 1)`, day2.answer1(readFile.toArray('./data/day_2/betdex.txt')));
console.log(`Day 2, Question 2)`, day2.answer2(readFile.toArray('./data/day_2/betdex.txt')));