import { answer1, answer2 } from "./day_1";

describe("Day 1", () => {
    const testData = [
        "1000",
        "2000",
        "3000",
        "",
        "4000",
        "",
        "5000",
        "6000",
        "",
        "7000",
        "8000",
        "9000",
        "",
        "10000",
    ];
    it("sample answer 1", () => {
        const result = answer1(testData);
        expect(result).toBe(24000);
    });
    it("sample answer 2", () => {
        const result = answer2(testData);
        expect(result).toBe(45000);
    });
});
