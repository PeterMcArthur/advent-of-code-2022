const sum = (num1: number, num2: string): number => num1 + parseInt(num2);

const isLastValue = <T>(arr: T[]) => (index: number) => arr.length === (index + 1);

type CalorieTracker = {
    highest: number;
    previousTotal: number;
}

export const answer1 = (data: string[]) => {
    const isArrayEnd = isLastValue(data);
    const { highest } = data.reduce(({ highest, previousTotal }, calorie, index): CalorieTracker => {
        if (calorie === "" || isArrayEnd(index)) {
            return {
                highest: highest < previousTotal ? previousTotal : highest,
                previousTotal: 0
            };
        } else {
            return {
                highest,
                previousTotal: sum(previousTotal, calorie),
            }
        }
    }, {
        highest: 0,
        previousTotal: 0,
    });

    return highest;
};

const getTop3 = (topElfs: number[]) => {
    const [a, b, c, ] = topElfs.sort((a,b) => b - a);
    return [a, b, c];
}

type Top3CalorieTracker = {
    top3: number[];
    previousElfTotal: number;
}

export const answer2 = (data: string[]) => {
    const isArrayEnd = isLastValue(data);
    const { top3 } = data.reduce(({ top3, previousElfTotal }, calorie, index): Top3CalorieTracker => {
        const elfCalorieCountComplete = calorie === "" || isArrayEnd(index);
        if (!elfCalorieCountComplete) {
            return {
                top3,
                previousElfTotal: sum(previousElfTotal, calorie),
            };
        }
        const currentTotal = isArrayEnd(index) ? sum(previousElfTotal, calorie) : previousElfTotal;
        const isNewTopElf = currentTotal > top3[2];
        if (!isNewTopElf) {
            return {
                top3,
                previousElfTotal: 0
            };
        }
        return {
            top3: getTop3([...top3, currentTotal]),
            previousElfTotal: 0
        }
    }, {
        top3: [0, 0, 0],
        previousElfTotal: 0,
    });

    return top3.reduce((total, val) => total + val);
};
