const rock = 1;
const paper = 2;
const sissors = 3;
const lose = 0;
const draw = 3;
const win = 6;

const roundOneKey = {
    "A X": draw + rock,
    "A Y": win + paper,
    "A Z": lose + sissors,
    "B X": lose + rock,
    "B Y": draw + paper,
    "B Z": win + sissors,
    "C X": win + rock,
    "C Y": lose + paper,
    "C Z": draw + sissors,
}

const roundTwoKey = {
    "A X": lose + sissors,
    "A Y": draw + rock,
    "A Z": win + paper,
    "B X": lose + rock,
    "B Y": draw + paper,
    "B Z": win + sissors,
    "C X": lose + paper,
    "C Y": draw + sissors,
    "C Z": win + rock,
}

const calculateRound =  (map: typeof roundOneKey | typeof roundTwoKey) => (round: string) => map[round as keyof typeof map];
const add = (val1: number, val2: number) => val1 + val2;


export const answer1 = (data: string[]) =>
    data
        .map(calculateRound(roundOneKey))
        .reduce(add)


export const answer2 = (data: string[]) =>
    data
        .map(calculateRound(roundTwoKey))
        .reduce(add)