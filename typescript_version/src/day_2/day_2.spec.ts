import { answer1, answer2 } from "./day_2";

describe("Day 2", () => {
    const testData = [
        "A Y",
        "B X",
        "C Z",
    ];
    it("sample answer 1", () => {
        const result = answer1(testData);
        expect(result).toBe(15);
    });
    it("sample answer 2", () => {
        const result = answer2(testData);
        expect(result).toBe(12);
    });
});
